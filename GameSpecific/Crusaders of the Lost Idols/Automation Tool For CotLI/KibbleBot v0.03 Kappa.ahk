; KibbleBot - a simple AHK assistant for Crusaders of the Lost Idols on Steam
; version 0.03 Kappa
; http://twitch.tv/clickerheroesbot
;
; Changelog:
; 0.02 added formation selection
; 0.03 adjusted right loot coordinate, added more main crusaders
;
; Hotkeys:
; F6 - Level main crusader (configure the setting)
; F7 - Level all crusaders (requires Spend It All)
; F8 - Buy all upgrades (requires Upgrade Them All)
; F9 - Toggle between leveling all crusaders or main crusader
; F10 - Toggle auto-clicker
; F11 - Toggle auto-progress (toggles progress back and forth every 10 minutes)
; F12 - Toggle auto-loot (requires control of mouse)

; REQUIRED CONFIGURATION:
; Pick the slot number of your main crusader slot.
; 2-Jim, 3-Emo Werewolf, 4-Sasha, 5-Hermit, 6-Kaine, 7-Princess, 8-Natalie
; 9-Jason, 10-Artaxes, 11-Khouri, 12-Gryphon, 13-Sarah, 14-Panda, 15-Sal
; 16-Phoenix, 17-Reginald, 18-Thalia, 19-Merci, 20-Nate, 21-Exterminator, 22-Shadow Queen
; 19 through 22 require all 23 crusader slots unlocked

winScaling := 100		; Enter your windows display scaling amount. (100 = default)
mainCrusader := 20		; Pick main crusader slot number from the list above.
updateFormation := true	; Update formation? (true/false)
formLevel := 1			; Leveling formation (1, 2 or 3)
formMain := 1			; Main formation (1, 2 or 3)
				
; END CONFIGURATION

#SingleInstance force
#Persistent
SetTitleMatchMode 1
winName=Crusaders of The Lost Idols
SetControlDelay -1
version=KibbleBot v0.03 Kappa

; The following values can be adjusted if you know what you are doing:

SetTimer,LevelUp,60000		; level all crusaders interval (all crusaders mode)
SetTimer,LevelMain,15000	; level main crusader interval (main crusader mode)
SetTimer,PickUpLoot,500		; auto-loot collection speed
SetTimer,MonsterClick,200	; auto-clicker speed
SetTimer,AutoProgress,600000	; auto-progress toggle interval

; Locations:

xUpgrade := 985 * (winScaling / 100)
yUpgrade := 560 * (winScaling / 100)
yBuyAll := 650 * (winScaling / 100)
xScrollLeft := 16 * (winScaling / 100)
xScrollRight := 988 * (winScaling / 100)
yScroll := 610 * (winScaling / 100)
xLootLeft := 550 * (winScaling / 100)
xLootRight := 900 * (winScaling / 100)
yLoot := 350 * (winScaling / 100)
xClick := 800 * (winScaling / 100)
yClick := 440 * (winScaling / 100)
xForm1 := 180 * (winScaling / 100)
xForm2 := 212 * (winScaling / 100)
xForm3 := 244 * (winScaling / 100)
yForm := 500 * (winScaling / 100)
xMain := 600 * (winScaling / 100)
yMainTop := 550 * (winScaling / 100)
yMainBottom := 630 * (winScaling / 100)
numScroll := Ceil((mainCrusader - 4) / 2)
if(mainCrusader==2)
 {
 xMain := 300 * (winScaling / 100)
 }
if(mod(mainCrusader,2)==1)
 {
 yMain := yMainTop 
 }
if(mod(mainCrusader,2)==0)
 {
 yMain := yMainBottom 
 }
if(formLevel==1)
 {
 xFormLevel := xForm1
 }
if(formLevel==2)
 {
 xFormLevel := xForm2
 }
if(formLevel==3)
 {
 xFormLevel := xForm3
 }
if(formMain==1)
 {
 xFormMain := xForm1
 }
if(formMain==2)
 {
 xFormMain := xForm2
 }
if(formMain==3)
 {
 xFormMain := xForm3
 }

; Main script functions:

Pause::pause
return

AutoProgress:
If(!progress){
return
}
WinActivate, Crusaders of The Lost Idols
SendInput, g
return

PickUpLoot:
If(!lootpause){
return
}
WinActivate, Crusaders of The Lost Idols
MouseMove xLootLeft,yLoot
sleep 500
MouseMove xLootRight,yLoot
return

LevelUp:
If(!mode){
  ; Scroll Right
  loop, 10 {
  ControlClick,% "x" xScrollRight " y" yScroll,% winName,,,,Pos NA
  }
  if (updateFormation)
  {
  ControlClick,% "x" xFormLevel " y" yForm,% winName,,,,Pos NA   ; Update Formation
  }
  ; Upgrade Crusaders
  ControlClick,% "x" xUpgrade " y" yUpgrade,% winName,,,,Pos NA
  sleep 2500
  ; Level Up Crusaders
  ControlClick,% "x" xUpgrade " y" yBuyAll,% winName,,,,Pos NA
  }
return

LevelMain:
If(mode){
  ControlSend,, {CtrlDown}, ahk_class ApolloRuntimeContentWindow
  ControlClick,% "x" xMain " y" yMain,% winName,,,,Pos NA
  ControlSend,, {CtrlUp}, ahk_class ApolloRuntimeContentWindow
  if (updateFormation)
  {
  ControlClick,% "x" xFormMain " y" yForm,% winName,,,,Pos NA   ; Update Formation
  }
}
return

MonsterClick:
If(!clickpause){
return
}
ControlClick,% "x" xClick " y" yClick,% winName,,,,Pos NA
return

F6::
Gosub, LevelMain
return

F7::
ControlClick,% "x" xUpgrade " y" yBuyAll,% winName,,,,Pos NA
return

F8::
ControlClick,% "x" xUpgrade " y" yUpgrade,% winName,,,,Pos NA
return

F9::
If(mode) {
  loop, 10 {
  ControlClick,% "x" xScrollRight " y" yScroll,% winName,,,,Pos NA
  }
  mode:=!mode
  MsgBox, 4096, %version%, All Crusaders Mode Activated, 1
  return
 }
If(!mode) {
  if (mainCrusader < 19)
  {  
   loop, 10 {
   ControlClick,% "x" xScrollLeft " y" yScroll,% winName,,,,Pos NA
   }
   loop, %numScroll% {
   ControlClick,% "x" xScrollRight " y" yScroll,% winName,,,,Pos NA
   }
  }
  else if (mainCrusader > 20)
  {
   loop, 10 {
   ControlClick,% "x" xScrollRight " y" yScroll,% winName,,,,Pos NA
   }
  }
  else if (mainCrusader > 18)
  {
   loop, 10 {
   ControlClick,% "x" xScrollRight " y" yScroll,% winName,,,,Pos NA
   }
   ControlClick,% "x" xScrollLeft " y" yScroll,% winName,,,,Pos NA
  }
  mode:=!mode
  MsgBox, 4096, %version%, Main Crusader Mode Activated, 1
  return
  }
return

F10::
clickpause:=!clickpause
if(clickpause)
{
MsgBox, 4096, %version%, Clicker Enabled, 1
return
}
MsgBox, 4096, %version%, Clicker Disabled, 1
return

F11::
progress:=!progress
if(progress)
{
MsgBox, 4096, %version%, Auto-Progress Enabled, 1
return
}
MsgBox, 4096, %version%, Auto-Progress Disabled, 1
return

F12::
lootpause:=!lootpause
if(lootpause)
{
MsgBox, 4096, %version%, Auto-Loot Enabled, 1
return
}
MsgBox, 4096, %version%, Auto-Loot Disabled, 1
return