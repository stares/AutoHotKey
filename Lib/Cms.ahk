﻿ms(h:=0, m:=0, s:=0, ms:=0) {
  ; Used to ensure only integers are passed.
  chkArray    := {"hours":h, "minutes":m, "seconds":s, "milliseconds":ms}
  ; Loop through array
  for index, value in chkArray
  ; If it's not an integer
  if value is not Integer
  {
    ; throw an error and exit thread.
    MsgBox, % "Error! Your " index " value is not an integer."
    Exit
  }


  ; Calculate ms and save to result
  result  := (((((h*60) + m ) * 60) + s) * 1000) + ms

  ; Return result
  return result
}
