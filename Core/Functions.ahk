/*
Function Documentation: http://ahkscript.org/docs/Functions.htm

Examples:

Add(x, y)
{
  return x + y
}

Add(2, 3) ; Simply calls the function
MyNumber := Add(2, 3) ; Stores the value

*/

; Open VSCode to edit file
Edit(file) {
  global
  Run % Settings.EditorPath " " file
}

; Check if error level, and if so, show message box with message, and break
ifErrorMsg(title, text) {
  If (ErrorLevel) {
    MsgBox, 48, %title%, %text%
    Return
  }
}

; Open or Close program
openOrCloseProgram(programClass, path) {
  IfWinExist, ahk_class %programClass%
  {
    IfWinActive, ahk_class %programClass%
    {
      WinClose, ahk_class %programClass%
      Return
    }

    IfWinNotActive, ahk_class %programClass%
    {
      WinActivate, ahk_class %programClass%
      Return
    }
  }

  IfWinNotExist, ahk_class %programClass%
  {
    Run, "%path%", ,UseErrorLevel
    IfErrorMsg("Error", "Error: Could not find the path: " . path)
    Return
  }
}

; Open or Close directory
openOrCloseDir(windowName, directory) {
  IfWinExist, %windowName%
  {
    IfWinActive, %windowName%
    {
      WinClose, %windowName%
      Return
    }

    IfWinNotActive, %windowName%
    {
      WinActivate, %windowName%
      Return
    }
  }

  IfWinNotExist, %windowName%
  {
    Run, "%directory%", ,UseErrorLevel
    IfErrorMsg("Error", "Error: Could not find the directory: " . directory)
    Return
  }
}

; Open or Minimize program
openOrMinimizeProgram(programClass, path) {
  IfWinActive, ahk_class %programClass%
  {
    WinMinimize, ahk_class %programClass%
    Return
  }

  IfWinNotActive, ahk_class %programClass%
  {
    Run, %path%, ,UseErrorLevel
    IfErrorMsg("Error", "Error: Could not find the path:" . path)
    Return
  }
  Return
}

; Open or Activate program
openOrActivate(programClass, path) {
  IfWinExist, ahk_class %programClass%
  {
    IfWinNotActive, ahk_class %programClass%
    {
      WinActivate, ahk_class %programClass%
      Return
    }
    Return
  }

  IfWinNotExist, ahk_class %programClass%
  {
    Run, %path%, ,UseErrorLevel
    IfErrorMsg("Error", "Error: Could not find the path: " . path)
    Return
  }
  Return
}

; Open or Activate program (title)
openOrActivateTitle(programTitle, path) {
  IfWinExist, %programTitle%
  {
    IfWinNotActive, %programTitle%
    {
      WinActivate, %programTitle%
      Return
    }
    Return
  }

  IfWinNotExist, %programTitle%
  {
    Run, %path%, ,UseErrorLevel
    IfErrorMsg("Error", "Error: Could not find the path: " . path)
    Return
  }
  Return
}

; Open or Activate program (text)
openOrActivateText(programText, path) {
  IfWinExist, ,%programText%
  {
    IfWinNotActive, ,%programText%
    {
      WinActivate, ,%programText%
      Return
    }
    Return
  }

  IfWinNotExist, ,%programText%
  {
    Run, %path%, ,UseErrorLevel
    IfErrorMsg("Error", "Error: Could not find the path: " . path)
    Return
  }
  Return
}

; Saves clipboard, copies selected text, store in variable, return clipboard,
; then return the selected text
getSelectedText() {
  ; Save Clipboard to variable
  SavedClipBoard := ClipboardAll

  ; Clear Clipboard
  Clipboard := ""

  ; Copy the current highlighted text
  SendInput ^c

  ; Wait for Clipboard to contain something
  ClipWait, 2, 1
  IfErrorMsg("Error", "The attempt to copy text onto the clipboard failed.")

  ; Store Clipboard into variable
  Result := Clipboard

  ; Return Clipboard contents to original data
  Clipboard := SavedClipBoard

  ; Return selected text
  Return Result
}

convertCaseTo(case) {
  ; Save Clipboard to variable
  SavedClipBoard := ClipboardAll

  ; Clear Clipboard
  Clipboard := ""

  ; Copy the current highlighted text
  SendInput ^c

  ; Wait for Clipboard to contain something
  ClipWait, 2, 1
  IfErrorMsg("Error", "The attempt to copy text onto the clipboard failed.")

  ; Store Clipboard into variable
  Result := Clipboard

  if (case = "Title")
  {
    ; Convert string to title case (Title Case)
    StringUpper, Result, Result, T
  }else  if (case = "Upper")
   {
    ; Convert string to upper case (UPPERCASE)
    StringUpper, Result, Result
  }else  if (case = "Lower")
   {
    ; Convert string to lower case (lowercase)
    StringLower, Result, Result
  }else  {
    MsgBox Case not supported.
    Return
  }

  ; Set clipboard contents to converted case
  Clipboard := Result

  ; Paste back
  SendInput ^v

  Sleep, 1000 ; delay of 1s, or else you will end up pasting old clip content

  ; Return Clipboard contents to original data
  Clipboard := SavedClipBoard
  ClipWait, 2
  IfErrorMsg("Error", "Old clipboard content could not be retrieved. (Timeout of 2)")

  Return
}
