/*
Hotkey Documentation: http://ahkscript.org/docs/Hotkeys.htm

Examples:

  #n::Run, Notepad

  #n::
  Run, Notepad
Return

Modifiers:
^ - Ctrl
! - Alt
+ - Shift
# - Win

*/

; Toggle AHK Suspend (CTRL + ALT + F12)
^!F12::Suspend

; Reload entire script (CTRL + ALT + R)
^!r::Reload

; Converts selected text to title case (WIN + SHIFT + T)
#+t::CaseToTitle()

; Converts selected text to uppercase (WIN + SHIFT + U)
#+u::CaseToUpper()

; Converts selected text to lowercase (WIN + SHIFT + L)
#+l::CaseToLower()

; Minimizes current window (WIN + M)
#m::WinMin()

; Open or close Downloads folder (CTRL + WIN + L)
^#l::openDownloadDir()

; Open or close My Documents folder (CTRL + WIN + D)
^#d::openDocumentDir()

; Open or close My Pictures folder (CTRL + WIN + P)
^#p::openPicturesDir()

; Open or close My Videos folder (CTRL + WIN + Z)
^#z::openVideosDir()

; Open or close DropBox (CTRL + WIN + B)
;^#b::openDropBox()
